$(document).ready(function(){
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$("#add-user").click(function(){
		$('#username-span').text('');
		$('#mobile-span').text('');
		$('#add-user-form').find("input[type=password],input[type=text],input[type=number]").val("");
		$('#add-usermodal').modal('show');
	});

	$('#add-user-form').on('submit', function(e){
		e.preventDefault();
		$('#username-span').text('');
		$('#mobile-span').text('');
		var formData = $('#add-user-form').serialize();
		$.ajax({
			type:'POST',
			url:'/add_user',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					location.reload();
				}else {
					$.each(data, function(key,value){
						if (value.indexOf('name') >= 0) {
							$('#username-span').text(value);
						}
						if (value.indexOf('mobile') >= 0) {
							$('#mobile-span').text(value);
						}						
					});
				}
			},
			error:function(xhr,error,status){

			}
		});
	});

	$('.change-pass').on('click',function(){
		$('#change-password-form').find("input[type=password]").val("");
		$('.text-danger').text('');
		var user_id = $(this).data('id');
		$('#user_id').val(user_id);
		$('#change-password').modal('show');
	});

	$('#confirm-pass-id').on('keyup',function(){
		if ($(this).val().length == 0) {
			$('.text-danger').text('');
		}else{
			if ($('#new-pass-id').val() != $(this).val()) {
				$('.text-danger').text('passwords do not match');
				$('#change-pass-submit').prop('disabled',true);
			}else{
				$('.text-danger').text('Password Confirmed');
				$('#change-pass-submit').prop('disabled',false);
			}
		}
	});

	$('#change-password-form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#change-password-form').serialize();
		console.log(formData);
		$.ajax({
			type:'POST',
			url:'/change_pass',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					$('.sub-text').text(data.info);
					$('.popup-img').prop('src','img/sucessful.svg');
					$('#change-password').modal('hide');
					$('#sucess-modal').modal('show');
				}else if (data.msg == 0) {
					$('.sub-text').text(data.info);
					$('.popup-img').prop('src','img/failed.svg');
					$('#sucess-modal').modal('show');
				}
			},
			error:function(error){
				alert(error);
			}
		});
	});
	
	$('.delete-user').on('click', function(){
		var formData = 'user_id='+$(this).data('id');
		$.ajax({
			type:'POST',
			url:'delete_user',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					location.reload();
				}else if (data.msg == 2) {
					$('.sub-text').text(data.info);
					$('.popup-img').prop('src','img/failed.svg');
					$('#sucess-modal').modal('show');
				}
			},
			error:function(error){
				alert(error);
			}

		})
	});

	$('.notification-check').on('click', function(){
		if ($(this).val() == 'on') {
			$(this).prop('checked',false);
			$(this).val('');
			var formData = 'user_id='+$(this).attr('id')+'&change='+0;
			$.ajax({
				type:'POST',
				url:'/change_notification',
				data:formData,
				success:function(data){
					console.log(data);
				},
				error:function(error){
					console.log(error);
				}
			});
		}else{
			$(this).prop('checked',true);
			$(this).val('on');
			var formData = 'user_id='+$(this).attr('id')+'&change='+1;
			$.ajax({
				type:'POST',
				url:'/change_notification',
				data:formData,
				success:function(data){
					console.log(data);
				},
				error:function(error){
					console.log(error);
				}
			});
		}
	});	

	$('#switch-div4').on('click',function(){
		if ($(this).val() == 0) {
			$(this).prop('checked',true);
			$(this).val('1');
		}else{
			$(this).prop('checked',false);
			$(this).val('0');
		}
	});

	$('.edit-user').on('click',function(){
		var myId = $(this).data('id')
		var formData = 'id='+myId;
		$.ajax({
			type:'POST',
			url:'/get_user',
			data:formData,
			success:function(data){
				console.log(data);
				$('#edit_user_id').val(myId);
				$('#edit-name').val(data.val['name']);
				$('#edit-mobile').val(data.val['mobile']);
				$('#edit-usermodal').modal('show');
			},
			error:function(error){

			}
		})
	});

	$('#edit-user-form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#edit-user-form').serialize();
		$.ajax({
			type:'POST',
			url:'/update_user',
			data:formData,
			success:function(data){
					$('#edit-usermodal').modal('hide');
					$('.sub-text').text(data.info);
					$('.popup-img').prop('src','img/sucessful.svg');
					$('#change-password').modal('hide');
					$('#sucess-modal').modal('show');
			},
			error:function(error){
				console.log(error);
			}
		});
	});

});