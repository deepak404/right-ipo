$(document).ready(function(){
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	}); 
	$('.delete-ipo').on('click',function(){
		var formData = 'id='+$(this).data('id');
		$.ajax({
			type:'POST',
			url:'/delete_ipo',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					location.reload();
				}else{
					alert(data.info);				}
			},
			error:function(xhr,status,error){
				alert(error);
			},
		});
	});
	$('.rename-ipo').on('click', function(){
		var myId = $(this).data('id');
		var formData = 'id='+myId;
		$.ajax({
			type:'POST',
			url:'/get_report',
			data:formData,
			success:function(data){
				$('#rename-report-id').val(myId);
				$('#ipo-name').val(data.name);
				$('#rename-modal').modal('show');
			},
			error:function(xhr,status,error){
				alert(error);
			}
		});
	});
	$('#rename-form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#rename-form').serialize();
		$.ajax({
			type:'POST',
			url:'/rename_ipo',
			data:formData,
			success:function(data){
					$('#rename-modal').modal('hide');
					$('.sub-text').text(data.info);
					$('.popup-img').prop('src','img/sucessful.svg');
					$('#change-password').modal('hide');
					$('#sucess-modal').modal('show');
			},
			error:function(error){
				console.log(error);
			}
		});
	});
	var tr = $('tbody tr');
	$('.pointer-icon').on('click',function(){
		if ($(this).data('order') == 'asc') {
			$(this).data('order','desc'); 
			$(this).text('arrow_drop_up');
			$('.active-header').removeClass('active-header');
			$(this).parent().addClass('active-header');
			var data = $(this).data('type');
		    var numericallyOrderedDivs = tr.sort(function (a, b) {
		        return $(b).find("td."+data).text() - $(a).find("td."+data).text();
		    });
		}else{
			$(this).data('order','asc'); 
			$(this).text('arrow_drop_down');
			var data = $(this).data('type');
			$('.active-header').removeClass('active-header');
			$(this).parent().addClass('active-header');
		    var numericallyOrderedDivs = tr.sort(function (a, b) {
		        return $(a).find("td."+data).text() - $(b).find("td."+data).text();
		    });
		}
	    $('tbody').empty();
	    $.each(numericallyOrderedDivs, function(index, value){
	    	$('tbody').append(value);
	    })
	})
});