$(document).ready(function(){ 
	var report_id = '';
	var suscription = 0;
	if (document.getElementById('edit-report-id')) {
		report_id = $('#edit-report-id').val();
	}

	if ($('.completed-title').length == 6) {
		$('.custom-button').css('display','');
	}

	$('#issues-form,#edit-issues-form').on('mouseover', function(){
		$('li').removeClass('active-title');
		$('#issue-detail-list').addClass('active-title');
	});

	$('#company-profile-form').on('mouseover', function(){
		$('li').removeClass('active-title');
		$('#company-profile-list').addClass('active-title');
	});

	$('#company-financial-id').on('mouseover', function(){
		$('li').removeClass('active-title');
		$('#company-financial-list').addClass('active-title');
	});

	$('#promoter-from').on('mouseover', function(){
		$('li').removeClass('active-title');
		$('#promoter-list').addClass('active-title');
	});

	$('#valuation-form').on('mouseover', function(){
		$('li').removeClass('active-title');
		$('#val-rev-list').addClass('active-title');
	});

	$('#bottomline-form').on('mouseover', function(){
		$('li').removeClass('active-title');
		$('#bottomline-list').addClass('active-title');
	});

    $('#issues_from_date,#listing-date,#issues_to_date').datepicker();

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$('#issues-form').on('submit', function(e){
		e.preventDefault();
		var myUrl = '';
		if (report_id == '') {
			myUrl = '/add_issue';
			var formData = $('#issues-form').serialize();
		}else{
			myUrl = '/edit_issue'
			var formData = $('#issues-form').serialize()+'&report_id='+report_id;
		}
		$.ajax({
			type:'POST',
			url:myUrl,
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					report_id = data.report_id;
					$('.form-button').removeAttr('disabled');
					$('#issue_detail_comp').removeClass('titletick-color').addClass('completed-title');
					$('#report-count').text($('.completed-title').length);
					checkStatus()
					$('html, body').animate({
					    scrollTop: $("#company-profile-h").offset().top
					}, 500);
				}else if (data.msg == 0) {
					alert(data.info);
				}
			},
			error:function(xhr,error,status){
				console.log(error);
			},
		});
	});

	$('#edit-issues-form').on('submit', function(e){
		e.preventDefault();
		var formData = $('#edit-issues-form').serialize()+'&report_id='+report_id;
		$.ajax({
			type:'POST',
			url:'/edit_issue',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					$('#issue_detail_comp').removeClass('titletick-color').addClass('completed-title');
					$('#report-count').text($('.completed-title').length);
					checkStatus()
					$('html, body').animate({
					    scrollTop: $("#company-profile-h").offset().top
					}, 500);
				}else if (data.msg == 0) {
					alert(data.info);
				}
			},
			error:function(xhr,error,status){
				console.log(error);
			},
		});
	});
	
	$('#company-profile-form').on('submit', function(e){
		e.preventDefault();
		var formData = $('#company-profile-form').serialize()+'&report_id='+report_id;
		$.ajax({
			type:'POST',
			url:'/add_company',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					$('#company_profile_comp').removeClass('titletick-color').addClass('completed-title');
					$('#report-count').text($('.completed-title').length);
					checkStatus()
				}else if (data.msg == 0) {
					alert(data.info);
				}
			},
			error:function(xhr,error,status){
				console.log(error);
			},
		});
	});
	
	$('#no_of_years').on('keyup',function(){
		$('#financial_tbody').empty();
		var count = $('#no_of_years').val()
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		for (var i = 0; i < count; i++) {
			if (mm < 4) {
				yyyy -= 1;
				var date = '31-MAR-'+yyyy;
				$('#financial_tbody').append('<tr>'+
                                '<td><input type="text"  name="date'+i+'" class="table-input" value="'+date+'"></td>'+
                                '<td><input type="text" name="total_revenue'+i+'" class="table-input"  placeholder="Amount" required></td>'+
                                '<td><input type="text" name="total_expense'+i+'" class="table-input" placeholder="Amount" required></td>'+
                                '<td><input type="text" name="pat'+i+'" class="table-input" placeholder="Amount" required></td>'+
                            '</tr>');
			}else{
				var date = '31-MAR-'+yyyy;
				$('#financial_tbody').append('<tr>'+
                                '<td><input type="text"  name="date'+i+'" class="table-input" value="'+date+'"></td>'+
                                '<td><input type="text" name="total_revenue'+i+'" class="table-input"  placeholder="Amount" required></td>'+
                                '<td><input type="text" name="total_expense'+i+'" class="table-input" placeholder="Amount" required></td>'+
                                '<td><input type="text" name="pat'+i+'" class="table-input" placeholder="Amount" required></td>'+
                            '</tr>');
				yyyy -= 1;
			}
		}
	});
	
	$('#company-financial-id').on('submit', function(e){
		e.preventDefault();
		var formData = $('#company-financial-id').serialize()+'&report_id='+report_id;
		$.ajax({
			type:'POST',
			url:'/add_financial',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					$('#company_financials_comp').removeClass('titletick-color').addClass('completed-title');
					$('#report-count').text($('.completed-title').length);
					checkStatus()
				}else if (data.msg == 0) {
					alert(data.info);
				}
			},
			error:function(xhr,error,status){
				console.log(error);
			},
		});
	});
	
	$('#add-promoter-id').on('click',function(){
		var length = $('.promoter-class').length+1;
		var length1 = length-1;
		$('#add-promoter-div').append('<div class=" grp-spacing col-xs-12 col-sm-12 col-md-6 col-lg-6 promoter-class">'+      
                                        '<input type="text" name="promoter'+length1+'" class="input-mod" required>'+
                                        '<span class="bar"></span>'+
                                        '<label class="label">Promoter'+length+'</label>'+
                                    '</div>');
	});
	
	$('#promoter-from').on('submit',function(e){
		e.preventDefault();
		var length = $('.promoter-class').length;
		var formData = $('#promoter-from').serialize()+'&no_of_promoters='+length+'&report_id='+report_id;
		$.ajax({
			type:'POST',
			url:'/add_promoter',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					$('#promoter_comp').removeClass('titletick-color').addClass('completed-title');
					$('#report-count').text($('.completed-title').length);
					checkStatus()
				}else if (data.msg == 0) {
					alert(data.info);
				}
			},
			error:function(xhr,error,status){
				console.log(error);
			},
		});
	});
	
	$('#valuation-form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#valuation-form').serialize()+'&report_id='+report_id;
		$.ajax({
			type:'POST',
			url:'/add_valuation',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					$('#val_rev_comp').removeClass('titletick-color').addClass('completed-title');
					$('#report-count').text($('.completed-title').length);
					checkStatus()
				}else if (data.msg == 0) {
					alert(data.info);
				}
			},
			error:function(xhr,error,status){
				console.log(error);
			},
		});
	});
	
	$('.suscription-days').on('click',function(){
		$('#suscription-id').text($(this).text()+' ');
		$('#suscription-id').append('<span class="caret"></span>');
		suscription = $(this).data('value');
	});
	
	$('#bottomline-form').on('submit', function(e){
		e.preventDefault();
		var formData = $('#bottomline-form').serialize()+'&suscription='+suscription+'&report_id='+report_id;
		$.ajax({
			type:'POST',
			url:'/add_bottomline',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					$('#bottomline_comp').removeClass('titletick-color').addClass('completed-title');
					$('#report-count').text($('.completed-title').length);
					checkStatus()
				}else if (data.msg == 0) {
					alert(data.info);
				}
			},
			error:function(xhr,error,status){
				console.log(error);
			},
		});
	});
	
	function checkStatus(){
		var length = $('.completed-title').length;
		if (length == 6) {
			$('#sucess-modal').modal('show');
			$('.custom-button').css('display','');
		}
	}
	
	$('#object-issue').on('change', function(){
		if ($('#object-issue').is(':checked')) {
			$('#offer-check').text('check_box_outline_blank');
			$('#offers-div-id').empty();
		}else{
			$('#offers-div-id').empty();
			$('#offer-check').text('check_box');
			$('#offers-div-id').append('<textarea rows="2" class="input-mod" name="offers" id="offers-id" required></textarea>'+
                        '<span class="bar"></span>'+
                        '<label class="label">Offer for sale</label>'+
                        '<p class="pull-right text-count"><span id="offers-count">0</span></p>');
		}
	});
	
	$('#object-issue1').on('change', function(){
		if ($('#object-issue1').is(':checked')) {
			$('#fresh-check').text('check_box_outline_blank');
			$('#fresh-div-id').empty();
		}else{
			$('#fresh-div-id').empty();
			$('#fresh-check').text('check_box');
			$('#fresh-div-id').append('<textarea rows="2" class="input-mod" name="fresh_issue" id="fresh-id" required></textarea>'+
                                    '<span class="bar"></span>'+
                                    '<label class="label">Fresh Issue</label>'+
                                    '<p class="pull-right text-count"><span id="fresh-count">0</span></p>');
		}
	});
	
	$('.ipo-input').on('keyup',function(){
		$('#ipo-name-count').text($('.ipo-input').val().length);
	});

	$('#book-input-id').on('keyup',function(){
		$('#bool-count').text($('#book-input-id').val().length);
	});

	var book = document.querySelector('#book-input-id');

	book.addEventListener('keydown', autosize);
	             
	
	$('#company-input-id').on('keyup',function(){
		$('#about-company-id').text($('#company-input-id').val().length);
	});

	var company = document.querySelector('#company-input-id');

	company.addEventListener('keydown', autosize);
	
	$('#financial-detail-id').on('keyup',function(){
		$('#financial-detail-count').text($('#financial-detail-id').val().length);
	});

	var financial = document.querySelector('#financial-detail-id');

	financial.addEventListener('keydown', autosize);
	
	$(document).on('keyup','#offers-id',function(){
		$('#offers-count').text($('#offers-id').val().length);
	});	

	// var offer = document.querySelector('#offers-id');

	// offer.addEventListener('keydown', autosize);
	
	$(document).on('keyup','#fresh-id',function(){
		$('#fresh-count').text($('#fresh-id').val().length);
	});

	// var fresh = document.querySelector('#fresh-id');

	// fresh.addEventListener('keydown', autosize);
	
	$('#valuation-detail-id').on('keyup',function(){
		$('#valuation-detail-count').text($('#valuation-detail-id').val().length);
	});

	var valuation = document.querySelector('#valuation-detail-id');

	valuation.addEventListener('keydown', autosize);
	
	$('#review-id').on('keyup',function(){
		$('#review-count').text($('#review-id').val().length);
	});

	var review = document.querySelector('#review-id');

	review.addEventListener('keydown', autosize);
	
	$('#bottomline-id').on('keyup',function(){
		$('#bottomline-count').text($('#bottomline-id').val().length);
	});

	var bottomline = document.querySelector('#bottomline-id');

	bottomline.addEventListener('keydown', autosize);

	function autosize(){
	  var el = this;
	  setTimeout(function(){
	    el.style.cssText = 'height:auto;';
	    el.style.cssText = 'height:' + el.scrollHeight + 'px';
	  },0);
	}

	$('#download-id').on('click', function(){
		window.open('/exportpdf/'+report_id, '_blank');
	});

	$('#preview-id').on('click', function(){
		window.open('/showpdf/'+report_id, '_blank');
	});

});