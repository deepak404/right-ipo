$(document).ready(function(){ 
	$('#toggle-icon').on('click', function(){
		if ($(this).data('state') == 'close') {
			$('.minibar-active').removeClass('minibar-active');
			$('.minibar-logo').removeClass('minibar-logo');
			$('.minimenu-active').removeClass('minimenu-active');
			$('.minibaractive-canvas').removeClass('minibaractive-canvas');
			$('.minimain-canvas').removeClass('minimain-canvas');
			$(this).data('state','open');
		}else{
			$('.side-menulogo').addClass('minimenu-active');
			$('.sidemenu-spacing').addClass('minimenu-active');
			$('.logo').addClass('minibar-logo');
			$('.canvas').addClass('minibaractive-canvas');
			$('.main-canvas').addClass('minimain-canvas');
			$('.side-bar').addClass('minibar-active')
			$(this).data('state','close');
		}
	});
});