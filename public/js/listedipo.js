$(document).ready(function(){ 
	var tr = $("tbody tr");
	console.log(tr)
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$('#add-listed-ipo').on('click', function(){
		$.ajax({
			type:'GET',
			url:'/get_listed',
			success:function(data){
				$('#ipo-name-list').empty();
                $('#ipo-name-list').append('<option value="" disabled selected>Select IPO</option>');
				$.each(data.ipos, function(key, value){
					$('#ipo-name-list').append('<option value="'+value['id']+'">'+value['ipo_name']+'</option>');
				});
				$('#add-ipo').modal('show');
			},
			error:function(xhr,error,status){
				
			}
		})
	});

	$('#add-ipo-form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#add-ipo-form').serialize()
		$.ajax({
			type:'POST',
			url:'/add_listed',
			data:formData,
			success:function(data){
				if (data.msg == 1) {
					location.reload()
				}
			},
			error:function(error){
				alert(error);
			}
		});
	});

	$(".edit-listed").on('click',function(){
		$('#open-id').val($(this).data('id'));
		$('#opening-id').val('');
		$('#openprice-editmodal1').modal('show');
	});

	$('#op-editform').on('submit',function(e){
		var formData = $('#op-editform').serialize(); 
		e.preventDefault();
		$.ajax({
			type:'POST',
			url:'/update_opening',
			data:formData,
			success:function(data){
				location.reload();
			},
			error:function(error){
				console.log(error);
			}
		});
	});
	
	$('.pointer-icon').on('click',function(){
		if ($(this).data('order') == 'asc') {
			$(this).data('order','desc'); 
			$(this).text('arrow_drop_up');
			$('.active-header').removeClass('active-header');
			$(this).parent().addClass('active-header');
			var data = $(this).data('type');
		    var numericallyOrderedDivs = tr.sort(function (a, b) {
		        return $(b).find("td."+data).text() - $(a).find("td."+data).text();
		    });
		}else{
			$(this).data('order','asc'); 
			$(this).text('arrow_drop_down');
			var data = $(this).data('type');
			$('.active-header').removeClass('active-header');
			$(this).parent().addClass('active-header');
		    var numericallyOrderedDivs = tr.sort(function (a, b) {
		        return $(a).find("td."+data).text() - $(b).find("td."+data).text();
		    });
		}
	    $('tbody').empty();
	    $.each(numericallyOrderedDivs, function(index, value){
	    	$('tbody').append(value);
	    })
	})


});
