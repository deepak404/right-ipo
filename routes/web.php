<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});

Route::get('/', 'HomeController@index');

Route::get('/get_listed','HomeController@getListed');

Route::get('/listed_ipo', 'HomeController@listedIpo');

Route::get('/reports','HomeController@reports');

Route::post('/delete_ipo','HomeController@deleteIpo');

Route::get('/settings','HomeController@setting');

Route::get('/edit_ipo/{id}','HomeController@editIpo');

Route::get('/create_report','HomeController@createReport')->name('create_report');

Route::post('/add_issue','HomeController@addIssue');

Route::post('/edit_issue','HomeController@editIssue');

Route::post('/add_company','HomeController@addCompany');

Route::post('/add_financial','HomeController@addFinancial');

Route::post('/add_promoter','HomeController@addPromoter');

Route::post('/add_valuation','HomeController@addValuation');

Route::post('/add_bottomline','HomeController@addBottomline');

Route::post('/add_listed','HomeController@addListed');

Route::get('/export/upcoming','HomeController@exportUpcoming');
			
Route::get('/export/listed','HomeController@exportListed');

Route::post('/add_user','HomeController@addUser');

Route::post('/change_pass','HomeController@changePass');

Route::get('/exportpdf/{id}', 'HomeController@exportPdf');

Route::get('/showpdf/{id}', 'HomeController@showPdf');

Route::post('/delete_user', 'HomeController@deleteUser');

Route::post('/change_notification', 'HomeController@changeNotification');

Route::post('/get_user','HomeController@getUser');

Route::post('/update_user','HomeController@updateUser');

Route::post('/get_report','HomeController@getReport');

Route::post('/rename_ipo','HomeController@renameIpo');

Route::post('/update_opening','HomeController@updateOpening');

// Route::get('/test','HomeController@testtext');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

