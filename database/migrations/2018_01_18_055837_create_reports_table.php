<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('ipo_name');
            $table->date('issue_open_from');
            $table->date('issue_open_to');
            $table->string('issue_type');
            $table->string('issue_size');
            $table->string('face_value');
            $table->integer('price_band_from');
            $table->integer('price_band_to');
            $table->string('mcq');
            $table->string('listing_at');
            $table->date('listing_date');
            $table->text('book_running');
            $table->double('cut_of_price');
            $table->text('about_company');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
