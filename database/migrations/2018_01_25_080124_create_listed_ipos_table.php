<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListedIposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listed_ipos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id');
            $table->double('open_price');
            $table->string('index_name');
            $table->string('index_code');
            $table->double('premium');
            $table->double('today_price');
            $table->double('returns');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listed_ipos');
    }
}
