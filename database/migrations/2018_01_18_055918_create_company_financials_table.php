<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyFinancialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_financials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id');
            $table->string('date');
            $table->string('total_revenue');
            $table->string('total_expense');
            $table->string('pat');
            $table->text('financial_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_financials');
    }
}
