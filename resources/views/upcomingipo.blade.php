<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<title>RightIPO</title>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/global.css?V1.1" >
        <link rel="stylesheet" href="css/upcomingipo.css?V1.1" >
	</head>

    <body class="body">
        <section>
            <div class="wrapper col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class=" col-xs-2 col-sm-2 col-md-2 col-lg-2 side-bar">
                    <div class="side-menulogo" id="">
                        <a href="#" id="toggle-icon" data-state="open" class="menu-txt"><i class="material-icons logomenu-icon menu-icon">menu</i></a>
                        <img src="img/Rightipo_logo.svg" class="logo">
                    </div>
                    <ul class="p-0">
                        <li class="list-inline sidemenu-spacing  side-menu  side-menu-active">
                            <a href="/" class="menu-txt   ">
                                <i class="material-icons menu-icon ">trending_up</i><span>Upcoming IPOs</span>
                            </a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu  ">
                            <a href="/listed_ipo" class="menu-txt  "><i class="material-icons menu-icon">clear_all</i><span>Listed IPOs</span></a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu  ">
                            <a href="/reports" class="menu-txt "><i class="material-icons menu-icon">description</i><span>Report</span></a>
                        </li>

                        <?php if (\Auth::user()->role == 1): ?>
                            <li class="list-inline sidemenu-spacing side-menu  ">
                                <a href="/settings" class="menu-txt "><i class="material-icons menu-icon">settings</i><span>settings</span></a>
                            </li>
                        <?php endif ?>
                        <li class="list-inline sidemenu-spacing side-menu " id="logout-menu">
                            <a href="/logout" class="menu-txt"><i class="material-icons menu-icon" id="logout-icon">exit_to_app</i><span>Logout</span></a>
                        </li>
                    </ul>   
                </div>

                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 plr-0 main-canvas">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title-spacing" id="title-border">
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 pl-0">
                            <h2 class="title">Upcoming IPOs</h2>
                        </div>  
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 plr-0">
                            <a href="/export/upcoming" class="custom-button">
                            <i class="material-icons round-buttons">file_download</i><h4 class="button-suptxt"> Export</h4>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0 table-spacing scroll" id="global-scroller">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>IPO Name </th>
                                    <th >Issue Open</th>
                                    <th>Size <i data-order='asc' data-type='issue_size' class="material-icons arrow-down pointer-icon">arrow_drop_down</i></th>
                                    <th>Valuation P/E</th>
                                    <th>Listing Date <i data-order='asc' data-type='listing_date' class="material-icons arrow-down pointer-icon">arrow_drop_down</i></th>
                                    <th>Cut of price<i data-order='asc' data-type='cut_of_price' class="material-icons arrow-down pointer-icon">arrow_drop_down</i></th>
                                    <th>Subscription <i data-order='asc' data-type='sub' class="material-icons arrow-down pointer-icon">arrow_drop_down</i></th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($ipos as $ipo)
                                <tr>
                                    <td>{{$ipo['ipo_name']}}</td>
                                    <td>{{$ipo['issue_open_from']}} - {{$ipo['issue_open_to']}}</td>
                                    <td class="issue_size">{{$ipo['issue_size']}}</td>
                                    <td>{{$ipo['valuation_pe']}}</td>
                                    <td class="listing_date">{{$ipo['listing_date']}}</td>
                                    <td class="cut_of_price">{{$ipo['cut_of_price']}}</td>
                                    <td><span class="sub">{{$ipo['sub']}}%</span>
                                        <button class="btn btn-default dropdown-mod dropdown-toggle recent-icondd pull-right dropdown-icon" type="button" data-toggle="dropdown">
                                            <i class="material-icons pull-right recent-icon">more_vert</i>
                                        </button>
                                        <ul class="dropdown-menu vert-moredd">
                                            <li><a href="\exportpdf\{{$ipo['id']}}">Download Report</a></li>
                                            <li><a href="#" class="delete-ipo" data-id="{{$ipo['id']}}">Remove</a></li>
                                        </ul>    
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <script type="text/javascript" src="js/global.js?V1.1"></script>
        <script type="text/javascript" src="js/report.js?V1.1"></script>
    </body>
</html>