<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<title>RightIPO</title>
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/global.css?V1.1" >
        <link rel="stylesheet" href="css/settings.css?V1.1" >
    </head>

    <body class="body">
        <section>
            <div class="wrapper col-lg-12">
                <div class=" col-xs-2 col-sm-2 col-md-2 col-lg-2 side-bar">
                    <div class="side-menulogo" id="">
                        <a href="#" id="toggle-icon" data-state="open" class="menu-txt"><i class="material-icons logomenu-icon menu-icon">menu</i></a>
                        <img src="img/Rightipo_logo.svg" class="logo" id=" ">
                    </div>
                    <ul class="p-0">
                        <li class="list-inline sidemenu-spacing  side-menu  ">
                            <a href="/" class="menu-txt   ">
                                <i class="material-icons menu-icon ">trending_up</i><span>Upcoming IPOs</span>
                            </a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu  ">
                            <a href="/listed_ipo" class="menu-txt  "><i class="material-icons menu-icon">clear_all</i><span>Listed IPOs</span></a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu  ">
                            <a href="/reports" class="menu-txt "><i class="material-icons menu-icon">description</i><span>Report</span></a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu  side-menu-active">
                            <a href="/settings" class="menu-txt "><i class="material-icons menu-icon">settings</i><span>settings</span></a>
                        </li>
                        <li class="list-inline sidemenu-spacing side-menu " id="logout-menu">
                            <a href="/logout" class="menu-txt"><i class="material-icons menu-icon" id="logout-icon">exit_to_app</i><span>Logout</span></a>
                        </li>
                    </ul>   
                </div>


                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 plr-0 main-canvas">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title-spacing" >
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 pl-0">
                            <h2 class="title">Settings</h2>
                        </div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 plr-0">
                            <button class="custom-button pull-left" data-toggle="modal" data-target="#myModal" id="add-user">
                            <i class="material-icons round-buttons" id="add-person">person_add</i><h4 class="button-suptxt"> Add User</h4>
                            </button>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0 table-spacing scroll" id="global-scroller">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Username </th>
                                    <th>Password</th>
                                    <th>Mobile No</th>
                                    <th>Notification</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$user['name']}}</td>
                                    <td>******</td>
                                    <td>{{$user['mobile']}}</td>
                                    <td>
                                        <?php if ($user['notification'] == 1): ?>
                                        <input type="radio" name="" value="on" class="notificatio-radio notification-check" id="{{$user['id']}}" checked>
                                        <label for="{{$user['id']}}" class="switch-bg">
                                            <span class="switch-btn"></span>
                                        </label> 
                                         <?php else: ?>
                                        <input type="radio" name="" value="" class="notificatio-radio notification-check" id="{{$user['id']}}" >
                                        <label for="{{$user['id']}}" class="switch-bg">
                                            <span class="switch-btn"></span>
                                        </label> 
                                        <?php endif ?>
                                            

                                        <button class="btn btn-default dropdown-mod dropdown-toggle recent-icondd pull-right dropdown-icon" type="button" data-toggle="dropdown">
                                            <i class="material-icons pull-right recent-icon">more_vert</i>
                                        </button>
                                        <ul class="dropdown-menu vert-moredd">
                                            <li class="change-pass" data-id="{{$user['id']}}"><a href="#">Change Password</a></li>
                                            <li class="edit-user" data-id="{{$user['id']}}"><a href="#">Edit</a></li>
                                            <li class="delete-user" data-id="{{$user['id']}}"><a href="#">Delete</a></li>
                                        </ul>    
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

    <!--Add User Modal -->
    <div id="add-usermodal" class="modal fade" role="dialog">
      <div class="modal-dialog popup-dialog ">
        <!-- Modal content-->
          <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="modal-header popup-header ">
                <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
                <h4 class="text-center">Add User</h4>
              </div>
              
              <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="add-user-form">
                    <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="text" name="name" class="input-mod" required>
                        <span class="bar"></span>
                        <label class="label">Username</label>
                        <span class="text-danger" id="username-span"></span>
                    </div>
                     <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="password" name="password" class="input-mod" required>
                        <span class="bar"></span>
                        <label class="label">Password</label>
                        <span class="text-danger" id="password-span"></span>
                    </div>
                     <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="number" name="mobile" class="input-mod" required>
                        <span class="bar"></span>
                        <label class="label">Mobile No</label>
                        <span class="text-danger" id="mobile-span"></span>
                    </div>
                    <div class=" lastgrp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <p class="pull-left notify-txt">Notification</p>
                        <div class="pull-right">
                            <input type="radio" name="notification" value="0" class="notificatio-radio" id="switch-div4" >
                            <label for="switch-div4" class="switch-bg">
                                <span class="switch-btn"></span>
                            </label> 
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12  col-md-12   col-lg-12 text-center">    
                        <input type="submit" value="Save" class="form-button"> 
                    </div>
                </form>
              </div>
            </div>
        </div>
      </div>

<!--edit User Modal -->
    <div id="edit-usermodal" class="modal fade" role="dialog">
      <div class="modal-dialog popup-dialog ">
        <!-- Modal content-->
          <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="modal-header popup-header ">
                <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
                <h4 class="text-center">Edit User</h4>
              </div>
              
              <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="edit-user-form">
                    <input type="hidden" name="id" value="" id="edit_user_id">
                    <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="text" name="name" class="input-mod" id="edit-name" required>
                        <span class="bar"></span>
                        <label class="label">Username</label>
                        <span class="text-danger" id="username-span"></span>
                    </div>
                     <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="number" name="mobile" class="input-mod" id="edit-mobile" required>
                        <span class="bar"></span>
                        <label class="label">Mobile No</label>
                        <span class="text-danger" id="mobile-span"></span>
                    </div>
                    <div class="col-xs-12 col-sm-12  col-md-12   col-lg-12 text-center">    
                        <input type="submit" value="Save" class="form-button"> 
                    </div>
                </form>
              </div>
            </div>
        </div>
      </div>




    <!--Add User Modal -->
    <div id="change-password" class="modal fade" role="dialog">
      <div class="modal-dialog popup-dialog ">
        <!-- Modal content-->
          <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="modal-header popup-header ">
                <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
                <h4 class="text-center">Change Password</h4>
              </div>
              
              <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="change-password-form">
                    <input type="hidden" name="id" value="" id="user_id">
                    <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="password" name="old-pass" class="input-mod" required>
                        <span class="bar"></span>
                        <label class="label">Old Password</label>
                    </div>
                     <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="password" name="new-pass" class="input-mod" id="new-pass-id" required>
                        <span class="bar"></span>
                        <label class="label">New Password</label>
                    </div>
                     <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="password" name="confirm-pass" class="input-mod" id="confirm-pass-id" required>
                        <span class="bar"></span>
                        <label class="label">Confirm Password</label>
                        <span class="text-danger"></span>
                    </div>

                    <div class="col-xs-12 col-sm-12  col-md-12   col-lg-12 text-center">    
                        <input type="submit" name="" value="Change" class="form-button" id="change-pass-submit" disabled="false"> 
                    </div>
                </form>
              </div>
            </div>
        </div>
      </div>
        <!-- Modal -->
        <div id="sucess-modal" class="modal fade" role="dialog">
            <div class="modal-dialog popup-dialog ">
            <!-- Modal content-->
              <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="modal-header popup-header ">
                    <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
                  </div>
                  
                  <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <img class="center-block popup-img"  src="img/sucessful.svg">
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <p class="sub-text">Report has completed and saved sucessfully</p>
                      <!-- <button class="popup-btn" type="submit">Okay</button> -->
                    </div>
                  </div>
                </div>
            </div>
        </div>
    <script type="text/javascript" src="js/settings.js?V1.1"></script>
    <script type="text/javascript" src="js/global.js?V1.1"></script>
    </body>
</html>