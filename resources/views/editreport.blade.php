<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>RightIPO</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{url('css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{url('css/global.css?V1.1')}}">
    <link rel="stylesheet" href="{{url('css/createreport.css?V1.1')}}">
</head>

<body class="body">
    <section>
        <div class="wrapper col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- menubar -->
            <div class="side-bar col-xs-2 col-sm-2 col-md-2 col-lg-2 ">
                <div class="list-inline side-menulogo">
                    <a href="#" id="toggle-icon" data-state="open" class="menu-txt"><i class="material-icons logomenu-icon menu-icon">menu</i></a>
                    <img src="{{url('img/Rightipo_logo.svg')}}" class="logo">
                </div>
                <ul class="p-0">
                    <li class="list-inline sidemenu-spacing  side-menu ">
                        <a href="/" class="menu-txt   ">
                                <i class="material-icons menu-icon ">trending_up</i><span>Upcoming IPOs</span>
                            </a>
                    </li>
                    <li class="list-inline sidemenu-spacing side-menu  ">
                        <a href="/listed_ipo" class="menu-txt  "><i class="material-icons menu-icon">clear_all</i><span>Listed IPOs</span></a>
                    </li>
                    <li class="list-inline sidemenu-spacing side-menu side-menu-active ">
                        <a href="/reports" class="menu-txt "><i class="material-icons menu-icon">description</i><span>Report</span></a>
                    </li>
                        <?php if (\Auth::user()->role == 1): ?>
                            <li class="list-inline sidemenu-spacing side-menu  ">
                                <a href="/settings" class="menu-txt "><i class="material-icons menu-icon">settings</i><span>settings</span></a>
                            </li>
                        <?php endif ?>
                    <li class="list-inline sidemenu-spacing side-menu " id="logout-menu">
                        <a href="/logout" class="menu-txt"><i class="material-icons menu-icon" id="logout-icon">exit_to_app</i><span>Logout</span></a>
                    </li>
                </ul>
            </div>
            <!-- sidebar end -->
            <div class="canvas col-xs-8 col-sm-8 col-md-8 col-lg-8 plr-0">
                <div class="col-lg-12 title-spacing">
                    <a href="/reports"><i class="material-icons head-icon">arrow_backward</i></a>
                    <h2 class="title"> Edit Reports</h2>
                </div>
                <div class="col-lg-12 plr-0 report-wrapper scroll-box" data-spy="scroll" data-target=".nav" data-offset="50" id="scroll-customize">
                    <h4 class="details-head ">Issue Details</h4>
                    <div class="form-wrapper">
                        <input type="hidden" name="id" value="{{$reports[0]['id']}}" id="edit-report-id">
                        <form class="col-xs-12 col-sm-12 col-md-12  col-lg-12" id="edit-issues-form">
                            <div class="grp-spacing lastgrp-spacing form-group col-xs-12 col-sm-12 col-md-12   col-lg-12 ">
                                <input type="text" name="ipo_name" class="ipo-input" value="{{$reports[0]['ipo_name']}}" required>
                                <span class="ipobar-bar"></span>
                                <label class="label iponame-text">Enter IPO name</label>
                                <p class="pull-right text-count"><span id="ipo-name-count">{{strlen($reports[0]['ipo_name'])}}</span></p>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
                                <p class="price-band">Issue Open</p>
                                <span> From <input name="open_date" class="input-mod price-bandinput" placeholder="Date" id="issues_from_date" value="{{$reports[0]['issue_open_from']}}" required> to
                                     <input name="close_date" class="input-mod price-bandinput" value="{{$reports[0]['issue_open_to']}}" id="issues_to_date" placeholder="Date" required></span>
                            </div>
                            <div class="form-group grp-spacing  col-xs-12 col-sm-12 col-md-6   col-lg-6 ">
                                <input type="text" name="issue_type" class="input-mod" value="{{$reports[0]['issue_type']}}" required>
                                <span class="bar"></span>
                                <label class="label">Issue Type</label>
                            </div>
                            <div class="form-group grp-spacing  col-xs-12 col-sm-12 col-md-6   col-lg-6 ">
                                <input type="number" step="any" name="issue_size" class="input-mod" value="{{$reports[0]['issue_size']}}" required>
                                <span class="bar"></span>
                                <label class="label">Issue Size Cr.</label>
                            </div>
                            <div class="form-group grp-spacing  col-xs-12 col-sm-12 col-md-6   col-lg-6 ">
                                <input type="text" name="face_val" value="{{$reports[0]['face_value']}}" class="input-mod" required>
                                <span class="bar"></span>
                                <label class="label">Face Value</label>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-6   col-lg-6 ">
                                <p class="price-band">Price band Per Equity Share</p>
                                <span> From <input type="number" step="any" name="price_from" value="{{$reports[0]['price_band_from']}}" class="input-mod price-bandinput" placeholder="Price" required> to <input type="number" step="any" name="price_to" value="{{$reports[0]['price_band_to']}}" class="input-mod price-bandinput" placeholder="Price" required></span>
                            </div>
                            <div class="form-group grp-spacing  col-xs-12 col-sm-12 col-md-6   col-lg-6 ">
                                <input type="text" value="{{$reports[0]['moq']}}" name="moq" class="input-mod" required>
                                <span class="bar"></span>
                                <label class="label">MOQ</label>
                            </div>
                            <div class="form-group grp-spacing  col-xs-12 col-sm-12 col-md-6   col-lg-6">
                                <input type="text" value="{{$reports[0]['listing_at']}}" name="listing_at" class="input-mod" required>
                                <span class="bar"></span>
                                <label class="label">Listing At</label>
                            </div>
                            <div class="form-group grp-spacing  col-xs-12 col-sm-12 col-md-6   col-lg-6">
                                <input id="listing-date" value="{{$reports[0]['listing_date']}}" name="listing_date" class="input-mod" required>
                                <span class="bar"></span>
                                <label class="label">Listing Date</label>
                            </div>
                            <div class="form-group grp-spacing  col-xs-12 col-sm-12 col-md-6   col-lg-6 ">
                                <input type="number" step="any" name="cut_price" class="input-mod" value="{{$reports[0]['cut_of_price']}}" required>
                                <span class="bar"></span>
                                <label class="label">Cut of Price</label>
                            </div>
                            <div class="form-group grp-spacing lastgrp-spacing col-xs-12 col-sm-12 col-md-12   col-lg-12">
                                <textarea class="input-mod" name="book" id="book-input-id" required>{{$reports[0]['book_running']}}</textarea>
                                <span class="bar"></span>
                                <label class="label">Book running lead manager</label>
                                <p class="pull-right text-count"><span id="bool-count">{{strlen($reports[0]['book_running'])}}</span></p>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                                <input type="submit" value="Save" class="form-button">
                            </div>
                        </form>
                    </div>
                    <h4 class="details-head" id="company-profile-h">Company Profile</h4>
                    <div class="form-wrapper">
                        <form class="col-xs-12 col-sm-12 col-md-12  col-lg-12" id="company-profile-form">
                            <div class="lastgrp-spacing col-xs-12 col-sm-12   col-md-12   col-lg-12 ">
                                <textarea rows="5" name="company_profile" class="input-mod" id="company-input-id" required>{{$reports[0]['about_company']}}</textarea>
                                <span class="bar"></span>
                                <label class="label">About Company</label>
                                <p class="pull-right text-count"><span id="about-company-id">{{strlen($reports[0]['about_company'])}}</span></p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12   col-lg-12 ">
                                <input type="submit" name="submit" value="Save" class="form-button" >
                            </div>
                        </form>
                    </div>
                    <h4 class="details-head">Company Financials</h4>
                    <div class="form-wrapper">
                        <form class="col-xs-12 col-sm-12 col-md-12  col-lg-12" id="company-financial-id">
                            <div class=" grp-spacing col-xs-12 col-sm-12  col-md-6   col-lg-6 ">
                                <input type="text" name="no_of_years" class="input-mod" id="no_of_years" id="no_of_years" value="{{count($financial_table)}}" required>
                                <span class="bar"></span>
                                <label class="label">NO. of Financial Years</label>
                            </div>
                            <div class="table-wrapper col-xs-12 col-sm-12  col-md-12   col-lg-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th> Particulars</th>
                                            <th>Total Revenue</th>
                                            <th>Total Expense</th>
                                            <th>Profit After Tax (PAT)</th>
                                        </tr>
                                    </thead>
                                    <tbody id="financial_tbody">
                                        @if($financial_table != '')
                                        <?php $count = 0; ?>
                                        @foreach($financial_table as $data)
                                        <tr>
                                            <td>
                                                <input type="text" name="date" class="table-input" value="{{$data['date']}}">
                                            </td>
                                            <td>
                                                <input type="text" name="total_revenue" class="table-input" value="{{$data['revenue']}}" placeholder="Amount" required>
                                            </td>
                                            <td>
                                                <input type="text" name="total_expense" class="table-input" value="{{$data['expense']}}" placeholder="Amount" required>
                                            </td>
                                            <td>
                                                <input type="text" name="pat" class="table-input" value="{{$data['pat']}}" placeholder="Amount" required>
                                            </td>
                                        </tr>
                                        <?php $count += 1; ?> 
                                        @endforeach
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="grp-spacing lastgrp-spacing col-xs-12 col-sm-12   col-md-12   col-lg-12 ">
                                <textarea rows="5" name="financial_details" class="input-mod" id="financial-detail-id" required>{{$financial_details}}</textarea>
                                <span class="bar"></span>
                                <label class="label">Financial Detail</label>
                                <p class="pull-right text-count"><span id="financial-detail-count">{{strlen($financial_details)}}</span></p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12   col-lg-12 ">
                                <input type="submit" name="" value="Save" class="form-button" >
                            </div>
                        </form>
                    </div>
                    <h4 class="details-head">Promoters</h4>
                    <div class="form-wrapper">
                        <form class="col-xs-12 col-sm-12 col-md-12  col-lg-12" id="promoter-from">
                            <div class="col-xs-12 col-sm-12 col-md-12  col-lg-12" id="add-promoter-div">
                                @if($promoter_names != '')
                                <?php $p_count = 0; ?>
                                @foreach($promoter_names as $promoter_name)
                                <div class=" grp-spacing col-xs-12 col-sm-12   col-md-6 col-lg-6 promoter-class">
                                    <input type="text" name="promoter{{$p_count}}" value="{{$promoter_name}}" class="input-mod" required>
                                    <span class="bar"></span>
                                    <label class="label">Promoter {{$p_count + 1}}</label>
                                </div>
                                <?php $p_count += 1; ?>
                                @endforeach
                                @else
                                <div class=" grp-spacing col-xs-12 col-sm-12   col-md-6 col-lg-6 promoter-class">
                                    <input type="text" name="promoter0" class="input-mod" required>
                                    <span class="bar"></span>
                                    <label class="label">Promoter 1</label>
                                </div>
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12">
                                <a href="#" id="add-promoter-id"><i class="material-icons link-icon">add_box</i><p class="link-txt"> Add Promoter</p></a>
                            </div>
                            <h4 class="details-head">Object of issues</h4>
                            <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12">
                                <input type="checkbox" name="offer_on" value="on" class="report-checkbox" id="object-issue" checked="true">
                                <label for="object-issue"><i id="offer-check" class="material-icons link-icon">check_box</i></label>
                                <p class="link-txt">Offer for sale</p>
                                <!-- text area after checked -->
                                <div id="offers-div-id" class="grp-spacing lastgrp-spacing col-xs-12 col-sm-12  col-md-12   col-lg-12  plr-0">
                                    <textarea rows="2" class="input-mod" name="offers"  id="offers-id" required>{{$issue_details}}</textarea>
                                    <span class="bar"></span>
                                    <label class="label">Offer for sale</label>
                                    <p class="pull-right text-count"><span id="offers-count">{{strlen($issue_details)}}</span></p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12">
                                <input type="checkbox" name="fresh_on" value="on" class="report-checkbox" id="object-issue1" checked="true">
                                <label for="object-issue1"><i id="fresh-check" class="material-icons link-icon">check_box</i></label>
                                <p class="link-txt">Fresh Issue</p>
                                <!-- text area after checked -->
                                <div id="fresh-div-id" class="grp-spacing lastgrp-spacing col-xs-12 col-sm-12 col-md-12   col-lg-12  plr-0">
                                    <textarea rows="2" class="input-mod" name="fresh_issue"  id="fresh-id" required>{{$fresh_issue}}</textarea>
                                    <span class="bar"></span>
                                    <label class="label">Fresh Issue</label>
                                    <p class="pull-right text-count"><span id="fresh-count">{{strlen($fresh_issue)}}</span></p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12  col-md-12   col-lg-12 ">
                                <input type="submit" name="" value="Save" class="form-button" >
                            </div>
                        </form>
                    </div>
                    <h4 class="details-head">Valuation & Review</h4>
                    <div class="form-wrapper">
                        <form class="col-xs-12 col-sm-12 col-md-12  col-lg-12" id="valuation-form">
                            <div class=" grp-spacing lastgrp-spacing col-xs-12 col-sm-12  col-md-12   col-lg-12 ">
                                <textarea name="valuation_details" rows="2" class="input-mod" id="valuation-detail-id" required>@if($val_rev != ''){{$val_rev['valuantion_details']}}@endif</textarea>
                                <span class="bar"></span>
                                <label class="label">Valuation & Details</label>
                                <p class="pull-right text-count"><span id="valuation-detail-count">
                                @if($val_rev != '')
                                {{strlen($val_rev['valuantion_details'])}}
                                @else
                                {{0}}
                                @endif</span></p>
                            </div>
                            <div class=" grp-spacing col-xs-12 col-sm-12 col-md-6   col-lg-6 ">
                                @if($val_rev != '')
                                <input type="text" name="valuation_pe" value="{{$val_rev['valuation_pe']}}" class="input-mod" required>
                                @else
                                <input type="text" name="valuation_pe" class="input-mod" required>
                                @endif
                                <span class="bar"></span>
                                <label class="label">Valuation P/E </label>
                            </div>
                            <div class=" grp-spacing lastgrp-spacing col-xs-12 col-sm-12  col-md-12   col-lg-12 ">
                                <textarea rows="4" class="input-mod" name="review" id="review-id" required>@if($val_rev != ''){{$val_rev['review']}}@endif</textarea>
                                <span class="bar"></span>
                                <label class="label">Review</label>
                                <p class="pull-right text-count"><span id="review-count">
                                @if($val_rev != '')
                                {{strlen($val_rev['review'])}}
                                @else
                                {{0}}
                                @endif</span></p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12   col-lg-12 ">
                                <input type="submit" name="" value="Save" class="form-button" >
                            </div>
                        </form>
                    </div>
                    <h4 class="details-head">Bottomline-Should you buy</h4>
                    <div class="form-wrapper">
                        <form class="col-xs-12 col-sm-12 col-md-12  col-lg-12" id="bottomline-form">
                            <div class=" grp-spacing lastgrp-spacing col-xs-12 col-sm-12  col-md-12   col-lg-12 ">
                                <textarea name="bottomline" rows="2" class="input-mod" id="bottomline-id" required>@if($bottomline != ''){{$bottomline['bottomlines']}}@endif</textarea>
                                <span class="bar"></span>
                                <label class="label">Bottomline-Should you buy</label>
                                <p class="pull-right text-count"><span id="bottomline-count">
                                @if($bottomline != '')
                                {{strlen($bottomline['bottomlines'])}}
                                @else
                                {{0}}
                                @endif
                                </span></p>
                            </div>
                            <div class=" grp-spacing col-xs-12 col-sm-12 col-md-6   col-lg-6 ">
                                <p class="price-band">Suscription</p>
                                <button class="btn btn-default dropdown-mod dropdown-toggle" id="suscription-id" type="button" data-toggle="dropdown">
                                    @if($bottomline != '')
                                    {{$bottomline['subscription']}}
                                    @else
                                    Day's 
                                    @endif
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li class="suscription-days" data-value="1">Day 1</li>
                                    <li class="suscription-days" data-value="2">Day 2</li>
                                    <li class="suscription-days" data-value="3">Day 3</li>
                                </ul>
                                @if($bottomline != '')
                                <input type="number" step="any" name="percentage" class="input-mod price-bandinput" value="{{$bottomline['percentage']}}" placeholder="Percentage" id="suscription-input" required>%
                                @else
                                <input type="number" step="any" name="percentage" class="input-mod price-bandinput" placeholder="Percentage" id="suscription-input" required>%
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12   col-lg-12 ">
                                <input type="submit" name="" value="Save" class="form-button" >
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 report-titlesbar plr-0">
                <h2 class="side-title">Reports 
                        <span class="pull-right"><span id="report-count">0</span>/6</span>  
                    </h2>
                <ul class="nav nav-pills nav-stacked title-list">
                        <li  class="active-title" id="issue-detail-list">
                            <p>Issue Details</p> 
                            <i class="material-icons completed-title" id="issue_detail_comp">check_circle</i>
                        </li>
                        <li id="company-profile-list">
                            <p>Company Profile</p> 
                            @if($reports[0]['about_company'] == '')
                            <i class="material-icons titletick-color" id="company_profile_comp">check_circle</i>
                            @else
                            <i class="material-icons completed-title" id="company_profile_comp">check_circle</i>
                            @endif
                        </li>
                        <li id="company-financial-list">
                            <p>Company Financials</p>
                            @if($financial_details == '')
                            <i class="material-icons titletick-color" id="company_financials_comp">check_circle</i>
                            @else
                            <i class="material-icons completed-title" id="company_financials_comp">check_circle</i>
                            @endif
                        </li>
                        <li id="promoter-list">
                            <p>Promoters & Object of Issues</p>
                            @if($promoter_names == '')
                            <i class="material-icons titletick-color" id="promoter_comp">check_circle</i>
                            @else
                            <i class="material-icons completed-title" id="promoter_comp">check_circle</i>
                            @endif
                        </li>
                        <li id="val-rev-list">
                            <p>Valuation & Review</p>
                            @if($val_rev == '')
                            <i class="material-icons titletick-color" id="val_rev_comp">check_circle</i>
                            @else
                            <i class="material-icons completed-title" id="val_rev_comp">check_circle</i>
                            @endif
                        </li>
                        <li id="bottomline-list">
                            <p>Bottomline-Should you buy</p>
                            @if($bottomline == '') 
                            <i class="material-icons titletick-color" id="bottomline_comp">check_circle</i>
                            @else
                            <i class="material-icons completed-title" id="bottomline_comp">check_circle</i>
                            @endif
                        </li>
                </ul>
                <button class="custom-button title-button" id="download-id" style="display: none;">
                    <i class="material-icons round-buttons">file_download</i>
                    <p class="button-suptxt">Download</p>
                </button>
                <button class="custom-button title-button" id="preview-id" style="display: none;">
                    <i class="material-icons round-buttons">fullscreen</i>
                    <p class="button-suptxt">Preview</p>
                </button>
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div id="sucess-modal" class="modal fade" role="dialog">
        <div class="modal-dialog popup-dialog ">
            <!-- Modal content-->
            <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="modal-header popup-header ">
                    <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
                </div>
                <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <img class="center-block popup-img" src="{{url('img/sucessful.svg')}}">
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <h4 class="text-left popup-text text-head">Report Completed</h4>
                        <p class="sub-text">Report has completed and saved sucessfully</p>
                        <!-- <button class="popup-btn" type="submit">Okay</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{url('js/createreport.js?V1.1')}}"></script>
    <script type="text/javascript" src="{{url('js/global.js?V1.1')}}"></script>
</body>

</html>