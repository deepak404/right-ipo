<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Right IPO</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/exportpdf.css">
</head>
<body>
	<header id="header-id">
		<img class="logo" src="img/pros.png">
		<div class="address-div">
			<p class="left-aling-text">Level 1, No 1, Balaji First Avenue, T.Nagar</p>
			<p class="left-aling-text">Chennai, Tamil Nadu 600017</p>
			<p class="left-aling-text">Ph: +91 9843051487</p>
		</div>
	</header>
	<div class="master-div">
		<h2>{{$reports[0]['ipo_name']}}</h2>
		<h4>ISSUE DETAILS</h4>
		<ul>
			<li class="issue-li">Issue Open: <span class="issue-list">{{$reports[0]['issue_open_from']}} - {{$reports[0]['issue_open_to']}}</span></li>
			<li class="issue-li">Issue Type: <span class="issue-list">{{$reports[0]['issue_type']}}</span></li>
			<li class="issue-li">Issue Size: <span class="issue-list">Rs. {{$reports[0]['issue_size']}} crore</span></li>
			<li class="issue-li">Face Value: <span class="issue-list">{{$reports[0]['face_value']}}</span></li>
			<li class="issue-li">Price Band: <span class="issue-list">Rs {{$reports[0]['price_band_from']}} - Rs {{$reports[0]['price_band_to']}} Per Equity Share</span></li>
			<li class="issue-li">MOQ: <span class="issue-list">{{$reports[0]['moq']}}</span></li>
			<li class="issue-li">Listing At: <span class="issue-list">{{$reports[0]['listing_at']}}</span></li>
			<li class="issue-li">Book running lead manager: <span class="issue-list">{{$reports[0]['book_running']}}</span></li>
		</ul>
		<h4>COMPANY PROFILE</h4>
		<p class="company-info">
			{{$reports[0]['about_company']}}
		</p>
		<h4>COMPANY FINANCIALS</h4>
		<?php if (count($financial_table) > 0): ?>
			<table>
				<thead>
					<tr>
						<th>Date</th>
						<th>Total Revenue</th>
						<th>Total Expense</th>
						<th>Profit After Tax (PAT)</th>
					</tr>
				</thead>
				<tbody>
					@foreach($financial_table as $selected)
					<tr>
						<td>{{$selected['date']}}</td>
						<td>{{$selected['revenue']}}</td>
						<td>{{$selected['expense']}}</td>
						<td>{{$selected['pat']}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		<?php endif ?>
		<p class="financial-info">
			{{$financial_details}}
		</p>
		<h4>PROMOTERS</h4>
		<ol>
			@foreach($promoter_names as $name)
			<li>{{$name}}</li>
			@endforeach
		</ol>
		<h4>OBJECT OF ISSUE</h4>
		<p class="issue-detail"><span class="issue-li">Offfer for Sale: </span>{{$issue_details}}</p>
		<p class="issue-detail"><span class="issue-li">Fresh Issue: </span>{{$fresh_issue}}</p>
		<h4>VALUATION</h4>
		<p class="issue-detail">{{$val_rev['valuantion_details']}}</p>
		<h4>REVIEW</h4>
		<p class="issue-detail">{{$val_rev['review']}}</p>
		<h4>BOTTOMLINE - SHOULD YOU BUY?</h4>
		<p class="issue-detail">{{$bottomline['bottomlines']}}</p>
		<p class="sub-days">Note: IPO was fully subscribed on Day {{$bottomline['subscription']}}.</p>
		<p class="listing-date">Listing Date: {{$reports[0]['listing_date']}}</p>
	</div>
<!-- 	<footer id="footer-id">
		<div class="footer-div">
			<p class="footer-p">Ph: +91 9843051487</p>
			<p class="footer-p">Email ID: vgupta@rightfunds.com</p>
		</div>
		<img class="footer-logo pull-right" src="img/pros.png">
	</footer> -->
</body>
</html>