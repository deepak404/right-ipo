<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<title>RightIPO</title>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/global.css?V1.1" >
        <link rel="stylesheet" href="css/listedipo.css?V1.1" >
	</head>

    <body class="body">
        <section>
            <div class="wrapper col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class=" col-xs-2 col-sm-2 col-md-2 col-lg-2 side-bar">
                    <div class="side-menulogo">
                        <a href="#" id="toggle-icon" data-state="open" class="menu-txt"><i class="material-icons logomenu-icon menu-icon">menu</i></a>
                        <img src="img/Rightipo_logo.svg" class="logo" id=" ">
                    </div>
                    <ul class="p-0">
                        <li class="list-inline sidemenu-spacing  side-menu ">
                            <a href="/" class="menu-txt   ">
                                <i class="material-icons menu-icon ">trending_up</i><span>Upcoming IPOs</span>
                            </a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu side-menu-active ">
                            <a href="/listed_ipo" class="menu-txt  "><i class="material-icons menu-icon">clear_all</i><span>Listed IPOs</span></a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu  ">
                            <a href="/reports" class="menu-txt "><i class="material-icons menu-icon">description</i><span>Report</span></a>
                        </li>

                        <?php if (\Auth::user()->role == 1): ?>
                            <li class="list-inline sidemenu-spacing side-menu  ">
                                <a href="/settings" class="menu-txt "><i class="material-icons menu-icon">settings</i><span>settings</span></a>
                            </li>
                        <?php endif ?>
                        <li class="list-inline sidemenu-spacing side-menu " id="logout-menu">
                            <a href="/logout" class="menu-txt  "><i class="material-icons menu-icon" id="logout-icon">exit_to_app</i><span>Logout</span></a>
                        </li>
                    </ul>   
                </div>

                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 plr-0 main-canvas ">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title-spacing" id="title-border">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 pl-0">
                            <h2 class="title">Listed IPOs</h2>
                        </div>
                         <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 pl-0">
                            <a href="#" class="custom-button pull-right" id="add-listed-ipo">
                            <i class="material-icons round-buttons">playlist_add</i><h4 class="button-suptxt"> Add Listed Ipo</h4>
                            </a>
                        </div>

                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 plr-0">
                            <a href="/export/listed" class="custom-button ">
                            <i class="material-icons round-buttons">file_download</i><h4 class="button-suptxt"> Export</h4>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0 table-spacing scroll" id="global-scroller">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Listing date <i data-order="asc" data-type="listed" class="material-icons arrow-down pointer-icon">arrow_drop_down</i> </th>
                                    <th>IPO Name </th>
                                    <th>Price Band</th>
                                    <th>Opening Price <i data-order="asc" data-type="open_price" class="material-icons pointer-icon">arrow_drop_down</i></th>
                                    <th>Premium <i data-order="asc" data-type="premium" class="material-icons pointer-icon">arrow_drop_down</i></th>
                                    <th>Todays Price<i data-order="asc" data-type="today_price" class="material-icons pointer-icon">arrow_drop_down</i></th>
                                    <th>Returns<i data-order="asc" data-type="return" class="material-icons pointer-icon">arrow_drop_down</i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($listed as $ipo)
                                <tr>
                                    <td class="listed">{{$ipo['date']}}</td>
                                    <td>{{$ipo['name']}}</td>
                                    <td>{{$ipo['price_from']}} - {{$ipo['price_to']}}</td>
                                    <td class="open_price">{{$ipo['open_price']}}<button class="custom-button edit-listed pull-right" data-id="{{$ipo['id']}}"><i class="material-icons edit-icon">mode_edit</i></button></td>
                                    <?php if ($ipo['premium'] >= 0): ?>
                                        <td style="color: green;" class="premium">{{$ipo['premium']}}</td>
                                        <?php else: ?>
                                        <td style="color: red;" class="premium">{{$ipo['premium']}}</td>
                                    <?php endif ?>
                                    <td class="today_price">{{$ipo['today_price']}}</td>
                                    <?php if ($ipo['return'] >= 0): ?>
                                        <td style="color: green;" class="return">{{$ipo['return']}}</td>
                                        <?php else: ?>
                                        <td style="color: red;" class="return">{{$ipo['return']}}</td>
                                    <?php endif ?>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

    <!-- Modal -->
    <div id="add-ipo" class="modal fade" role="dialog">
      <div class="modal-dialog popup-dialog">

        <!-- Modal content-->
        <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="modal-header popup-header">
            <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
            <h4 class="text-center">Add Listed IPO</h4>
          </div>
          <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form class="col-xs-12 col-sm-12 col-md-12 col-lg-12" name="add-ipo-form" id="add-ipo-form">
                <div class="grp-spacing  col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <select class="select-control" name="ipo-name" id="ipo-name-list" required>

                    </select>
                </div>
                <div class="grp-spacing  col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <select class="select-control" name="index" required>
                        <option value="" disabled selected>Select Index</option>
                        <option value="bse">BSE</option>
                        <option value="nse">NSE</option>
                    </select>
                </div>
                <div class=" grp-spacing  col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                    <input type="text" name="index_code" class="input-mod" required>
                    <span class="bar"></span>
                    <label class="label">Index Code</label>
                    <span class="text-danger" id="username-span"></span>
                </div>

                <div class=" grp-spacing lastgrp-spacing  col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                    <input type="number" step="any" name="open_price" class="input-mod" required>
                    <span class="bar"></span>
                    <label class="label">Opening Price</label>
                    <span class="text-danger" id="username-span"></span>
                </div>
             
                <div class="col-xs-12 col-sm-12  col-md-12   col-lg-12 text-center ">    
                    <input type="submit" value="Add" class="form-button"> 
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>


     <!--Add User Modal -->
    <div id="openprice-editmodal1" class="modal fade" role="dialog">
      <div class="modal-dialog popup-dialog ">
        <!-- Modal content-->
          <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="modal-header  popup-header ">
                <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
                <h4 class="text-center">Edit Opening Price</h4>
              </div>
              
              <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form class="col-xs-12 col-sm-12 col-md-12 col-lg-12" name="op-edit" id="op-editform">
                    <input type="hidden" name="id" value="" id="open-id">
                    <div class="lastgrp-spacing grp-spacing  col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="number" step="any" name="open_price" id="opening-id" class="input-mod" required>
                        <span class="bar"></span>
                        <label class="label">Opening Price</label>
                        <span class="text-danger" id="username-span"></span>
                    </div>
                     
                    <div class="col-xs-12 col-sm-12  col-md-12   col-lg-12 text-center">    
                        <input type="submit" value="Save" class="form-button"> 
                    </div>
                </form>
              </div>
            </div>
        </div>
      </div>


    <script type="text/javascript" src="js/global.js?V1.1"></script>
    <script type="text/javascript" src="js/listedipo.js?V1.1"></script>
    </body>
</html>