<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Right IPO</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/login.css" >
  </head>
  <body class="index-body">
    <div class="container ">
      <div class="row v-align">
        <div class="row card card-width center-block">
        <img class="login-logo center-block" src="img/Rightipo_logo.svg">
        <h3 class="text-center login-title">login</h3>
         <form  method="POST" action="{{ route('login') }}">
          {{ csrf_field() }}
          <div class="inputfield-group">
            <h5 class="label">Username</h5>
            <input class="login-textbox center-block" type="text" name="email" required>
          </div>
          <div  class="inputfield-group">
            <h5 class="label">Password</h5>
            <input class="login-textbox center-block" type="Password" name="password" required>
          </div>
          @if ($errors->has('email'))
              <span class="login-cases login-title failed">Login Failed</span> 
          @endif
          <button class="center-block login-button" type="submit" autofocus>Login</button>
       </form>
       <img class="abstract" src="img/abstract.svg">
      </div>
    </div>
    </div> 
  </body>
</html>