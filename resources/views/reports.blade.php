<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<title>RightIPO</title>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="css/global.css?V1.1" >
        <link rel="stylesheet" href="css/reports.css?V1.2" >
	</head>

    <body class="body">
        <section>
            <div class="wrapper col-lg-12">
                <div class=" col-xs-2 col-sm-2 col-md-2 col-lg-2 side-bar">
                    <div class="list-inline side-menulogo ">
                        <a href="#" id="toggle-icon" data-state="open" class="menu-txt"><i class="material-icons logomenu-icon menu-icon">menu</i></a>
                        <img src="img/Rightipo_logo.svg" class="logo" id=" ">
                    </div>
                    <ul class="p-0">
                        <li class="list-inline sidemenu-spacing  side-menu ">
                            <a href="/" class="menu-txt   ">
                                <i class="material-icons menu-icon ">trending_up</i><span>Upcoming IPOs</span>
                            </a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu  ">
                            <a href="/listed_ipo" class="menu-txt  "><i class="material-icons menu-icon">clear_all</i><span>Listed IPOs</span></a>
                        </li>

                        <li class="list-inline sidemenu-spacing side-menu side-menu-active ">
                            <a href="/reports" class="menu-txt "><i class="material-icons menu-icon">description</i><span>Report</span></a>
                        </li>

                        <?php if (\Auth::user()->role == 1): ?>
                            <li class="list-inline sidemenu-spacing side-menu  ">
                                <a href="/settings" class="menu-txt "><i class="material-icons menu-icon">settings</i><span>settings</span></a>
                            </li>
                        <?php endif ?>
                        <li class="list-inline sidemenu-spacing side-menu " id="logout-menu">
                            <a href="/logout" class="menu-txt"><i class="material-icons menu-icon" id="logout-icon">exit_to_app</i><span>Logout</span></a>
                        </li>
                    </ul>   
                </div>

                <div class="col-lg-10 plr-0 main-canvas">
                    <div class="col-lg-12 title-spacing">
                        <h2 class="title">Reports</h2>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-11 card-spacing">
                            <div class="create-card">
                                <a href="/create_report" class="create-txt ">
                                    <i class="material-icons round-icon">note_add</i><h4 class="report-txt">Create IPO Report</h4>
                                    <i class="material-icons pull-right forward-icon">arrow_forward</i> 
                                </a>
                            </div>
                            <h4 class="recent-headtext">Recently Created</h4>
                            <div class="recent-wrapper" id="global-scroller">
                                
                                <h5 class="recent-datetext">Today</h5>
                                @foreach($reports as $report)
                                <div class="recent-list">
                                    <h4 class="recent-txt">{{$report['ipo_name']}}</h4>
                                    <button class="btn btn-default dropdown-mod dropdown-toggle recent-icondd pull-right" type="button" data-toggle="dropdown">
                                        
                                    <i class="material-icons pull-right recent-icon">more_vert</i>
                                      </button>
                                      <ul class="dropdown-menu vert-moredd">
                                        <li><a href="\exportpdf\{{$report['id']}}"> Download</a></li>
                                        <li class="rename-ipo" data-id="{{$report['id']}}"><a href="#">Rename</a></li>
                                        <li class="delete-ipo" data-id="{{$report['id']}}"><a href="#">Delete</a></li>
                                      </ul> 
                                    <a href="/edit_ipo/{{$report['id']}}"><i class="material-icons pull-right recent-icon">mode_edit</i></a>
                                    
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <i class="material-icons upward">arrow_upward</i> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
 <!-- Rename modal -->

    <div id="rename-modal" class="modal fade" role="dialog">
      <div class="modal-dialog popup-dialog ">
        <!-- Modal content-->
          <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="modal-header popup-header ">
                <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
                <h4 class="text-center">Rename</h4>
              </div>
              
              <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="rename-form">
                    <input type="hidden" name="id" value="" id="rename-report-id">
                    <div class=" grp-spacing col-xs-12 col-sm-12 col-md-12 col-lg-12 ">      
                        <input type="text" name="ipo_name" class="input-mod" id="ipo-name" required>
                        <span class="bar"></span>
                        <label class="label">Enter name of IPO</label>
                    </div>
                    <div class="col-xs-12 col-sm-12  col-md-12   col-lg-12 text-center">    
                        <input type="submit" name="" value="Save" class="form-button"> 
                    </div>
                </form>
              </div>
            </div>
        </div>
      </div>
      <!-- Success Modal -->
        <div id="sucess-modal" class="modal fade" role="dialog">
            <div class="modal-dialog popup-dialog ">
            <!-- Modal content-->
              <div class="modal-content popup-card col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="modal-header popup-header ">
                    <button type="button" class="close" data-dismiss="modal"><i class="material-icons">close</i></button>
                  </div>
                  
                  <div class="modal-body popup-body col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <img class="center-block popup-img"  src="img/sucessful.svg">
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                      <h4 class="text-left popup-text text-head">Report Completed</h4>
                      <p class="sub-text">Report has completed and saved sucessfully</p>
                      <!-- <button class="popup-btn" type="submit">Okay</button> -->
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/report.js?V1.1"></script>
        <script type="text/javascript" src="js/global.js?V1.1"></script>
    </body>
</html>