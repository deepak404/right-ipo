<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ListedIpo;

class PriceUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'price:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update ipo LTP.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ipos = ListedIpo::get()->toArray();
        foreach ($ipos as $value) {
            $open_price = $value['open_price'];
            if ($value['index_name'] == 'bse') {
                $data = file_get_contents('http://www.bseindia.com/stock-share-price/SiteCache/EQHeaderData.aspx?text='.$value['index_code']);
                $data = explode('#', $data);
                $data = explode(',', $data[6]);
                // $data[0] -> Pre-close
                // $data[1] -> today open
                // $data[2] -> today high
                // $data[3] -> today low
                // $data[4] -> today close
                $close = $data[4];
                $returns = (($close - $open_price)/$open_price)*100;
                $returns = round($returns, 2);
                $update = ListedIpo::where('report_id',$value['report_id'])->update(['today_price'=>$close,'returns'=>$returns]);

            }elseif ($value['index_name'] == 'nse') {
                $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$value['index_code'].'.NS?interval=1d');
                $data = json_decode($file);
                $values = $data->chart->result[0]->indicators->quote[0];
                if (!empty($values->close)) {
                    $count = count($values->close) - 1;
                    $close = round($values->close[$count], 2);
                    $returns = (($close - $open_price)/$open_price)*100;
                    $returns = round($returns, 2);
                    $update = ListedIpo::where('report_id',$value['report_id'])->update(['today_price'=>$close,'returns'=>$returns]);
                }
            }

        }
    }
}
