<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\User;
use App\Report;

class Notification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification for upcoming IPOs.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $datetime = new DateTime('tomorrow');
        $users = User::where('role','!=',1)->where('notification',1)->pluck('mobile')->toArray();
        $ipos = Report::where('listing_date',$datetime)->pluck('ipo_name')->toArray();
        $users = implode(",", $users);
        foreach ($ipos as $ipo) {
            $ch = curl_init();
            $user="naveen@rightfunds.com:Rightfunds20";
            $receipientno = $users;
            $senderID="RFIPOS";
            $msgtxt=$ipo." IPO will be listed tomorrow at 9.00AM."; 
            curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
            $buffer = curl_exec($ch);
            curl_close($ch);
        }
    }
}
