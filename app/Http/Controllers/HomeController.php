<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Report;
use App\CompanyFinancial;
use App\Promoter;
use App\ValuantionReview;
use App\Bottomline;
use App\ListedIpo;
use App\User;
use Carbon\Carbon;
use Excel;
use DateTime;
use Hash;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $today = Carbon::now();
        $today = date('Y-m-d', strtotime($today));

        $reports = Report::where('issue_open_to','>=',$today)->get()->toArray();
        $upcomingipo = [];
        $count = 0;
        foreach ($reports as $value) {
            $upcomingipo[$count]['id'] = $value['id'];
            $upcomingipo[$count]['ipo_name'] = $value['ipo_name'];
            $upcomingipo[$count]['issue_open_from'] = $value['issue_open_from'];
            $upcomingipo[$count]['issue_open_to'] = $value['issue_open_to'];
            $upcomingipo[$count]['issue_size'] = $value['issue_size'];
            $upcomingipo[$count]['listing_date'] = $value['listing_date'];
            $upcomingipo[$count]['cut_of_price'] = $value['cut_of_price'];
            $upcomingipo[$count]['valuation_pe'] = ValuantionReview::where('report_id',$value['id'])->value('valuation_pe');
            $upcomingipo[$count]['sub'] = Bottomline::where('report_id',$value['id'])->value('percentage');
            $count += 1;
        }
        return view('upcomingipo',['ipos'=>$upcomingipo]);
    }

    public function listedIpo()
    {
        $listed = ListedIpo::get()->toArray();
        $finalData = [];
        $count = 0;
        foreach ($listed as $ipo) {
            $report = Report::where('id',$ipo['report_id'])->get()->toArray();
            $finalData[$count]['id'] = $ipo['report_id'];
            $finalData[$count]['name'] = $report[0]['ipo_name'];
            $finalData[$count]['date'] = $report[0]['listing_date'];
            $finalData[$count]['price_from'] = $report[0]['price_band_from'];
            $finalData[$count]['price_to'] = $report[0]['price_band_to'];
            $finalData[$count]['open_price'] = $ipo['open_price'];
            $finalData[$count]['premium'] = $ipo['premium'];
            $finalData[$count]['today_price'] = $ipo['today_price'];
            $finalData[$count]['return'] = $ipo['returns'];
            $count += 1;
        }
        // dd($finalData);
        return view('listedipo',['listed'=>$finalData]);
    }

    public function createReport()
    {
        return view('createreport');
    }

    public function reports()
    {
        $reports = Report::all()->toArray();

        return view('reports',['reports'=>$reports]);
    }

    public function deleteIpo(Request $request)
    {
        $id = $request['id'];
        $reports = Report::where('id',$id)->delete();
        $CompanyFinancial = CompanyFinancial::where('report_id',$id)->delete();
        $Promoter = Promoter::where('report_id',$id)->delete();
        $ValuantionReview = ValuantionReview::where('report_id',$id)->delete();
        $Bottomline = Bottomline::where('report_id',$id)->delete();
        $listed = ListedIpo::where('report_id',$id)->delete();
        return response()->json(['msg'=>1]);

    }

    public function editIpo($id)
    {
        $reports = Report::where('id',$id)->get()->toArray();
        $CompanyFinancial = CompanyFinancial::where('report_id',$id)->get()->toArray();
        $Promoter = Promoter::where('report_id',$id)->get()->toArray();
        $ValuantionReview = ValuantionReview::where('report_id',$id)->get()->toArray();
        $Bottomline = Bottomline::where('report_id',$id)->get()->toArray();

        if (empty($CompanyFinancial)) {
            $financial_table = '';
            $financial_details = '';
        }else{
            $financial_date = $CompanyFinancial[0]['date'];
            $financial_date = explode('|', $financial_date);
            $financial_revenue = $CompanyFinancial[0]['total_revenue'];
            $financial_revenue = explode('|', $financial_revenue);
            $financial_expense = $CompanyFinancial[0]['total_expense'];
            $financial_expense = explode('|', $financial_expense);
            $financial_pat = $CompanyFinancial[0]['pat'];
            $financial_pat = explode('|', $financial_pat);

            $financial_count = count($financial_date)-1;
            $financial_table = [];
            for ($i=0; $i < $financial_count; $i++) { 
                $financial_table[$i]['date'] = $financial_date[$i];
                $financial_table[$i]['revenue'] = $financial_revenue[$i];
                $financial_table[$i]['expense'] = $financial_expense[$i];
                $financial_table[$i]['pat'] = $financial_pat[$i];
            }
            $financial_details = $CompanyFinancial[0]['financial_details'];
        }

        if (empty($Promoter)) {
            $promoterNames = '';
            $issue_details = '';
            $fresh_issue = '';
        }else{
            $promoter_names = $Promoter[0]['promoter_names'];
            $promoter_names = explode('|', $promoter_names);

            $promoter_count = count($promoter_names)-1;
            $promoterNames = [];
            for ($i=0; $i < $promoter_count; $i++) { 
                $promoterNames[$i] = $promoter_names[$i];
            }
            $issue_details = $Promoter[0]['issue_details'];
            $fresh_issue = $Promoter[0]['fresh_issue'];
        }
        if (empty($Bottomline)) {
            $bottomlines = '';
        }else{
            $bottomlines = $Bottomline[0];
        }
        if (empty($ValuantionReview)) {
            $ValuantionReview = '';
        }else{
            $ValuantionReview = $ValuantionReview[0];
        }
        // dd($Bottomline);

        return view('editreport',['reports'=>$reports,'financial_table'=>$financial_table,'financial_details'=>$financial_details,'promoter_names'=>$promoterNames,'issue_details'=>$issue_details,'fresh_issue'=>$fresh_issue,'val_rev'=>$ValuantionReview,'bottomline'=>$bottomlines]);
    }


    public function addIssue(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ipo_name' => 'required',
            'open_date' => 'required',
            'close_date' => 'required',
            'issue_type' => 'required',
            'issue_size' => 'required',
            'face_val' => 'required',
            'price_from' => 'required',
            'price_to' => 'required',
            'moq' => 'required',
            'listing_at' => 'required',
            'listing_date' => 'required',
            'cut_price' => 'required',
            'book' => 'required',
        ]);

        if ($validator->fails()) {
            var_dump($validator->errors());
            die();
        }

        $insert_report = Report::insert(['user_id'=>\Auth::user()->id,
                                        'ipo_name'=>$request['ipo_name'],
                                        'issue_open_from'=>date('Y-m-d', strtotime($request['open_date'])),
                                        'issue_open_to'=>date('Y-m-d', strtotime($request['close_date'])),
                                        'issue_type'=>$request['issue_type'],
                                        'issue_size'=>$request['issue_size'],
                                        'face_value'=>$request['face_val'],
                                        'price_band_from'=>$request['price_from'],
                                        'price_band_to'=>$request['price_to'],
                                        'moq'=>$request['moq'],
                                        'listing_at'=>$request['listing_at'],
                                        'listing_date'=>date('Y-m-d', strtotime($request['listing_date'])),
                                        'cut_of_price'=>$request['cut_price'],
                                        'book_running'=>$request['book'],
                                        'created_at'=>Carbon::now()
                                    ]);
        if ($insert_report) {
            $report_id = Report::where('ipo_name',$request['ipo_name'])->value('id');

            return response()->json(['msg'=>1,'report_id'=>$report_id]);
        }else{
            return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
        }
    }

    public function editIssue(Request $request)
    {
        $insert_report = Report::where('id',$request['report_id'])
                                        ->update(['user_id'=>\Auth::user()->id,
                                        'ipo_name'=>$request['ipo_name'],
                                        'issue_open_from'=>date('Y-m-d', strtotime($request['open_date'])),
                                        'issue_open_to'=>date('Y-m-d', strtotime($request['close_date'])),
                                        'issue_type'=>$request['issue_type'],
                                        'issue_size'=>$request['issue_size'],
                                        'face_value'=>$request['face_val'],
                                        'price_band_from'=>$request['price_from'],
                                        'price_band_to'=>$request['price_to'],
                                        'moq'=>$request['moq'],
                                        'listing_at'=>$request['listing_at'],
                                        'listing_date'=>date('Y-m-d', strtotime($request['listing_date'])),
                                        'cut_of_price'=>$request['cut_price'],
                                        'book_running'=>$request['book'],
                                    ]);
        if ($insert_report) {
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
        }
    }

    public function addCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'book' => 'company_profile',
        ]);

        if ($validator->fails()) {
            var_dump($validator->fails());
            die();
        }
        $insert_data = Report::where('id',$request['report_id'])->update(['about_company'=>$request['company_profile']]);

        if ($insert_data) {
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0, 'info'=>'Storing data has some problem.']);
        }
    }
    public function addFinancial(Request $request)
    {
        $date = '';
        $total_revenue = '';
        $total_expense = '';
        $pat = '';
        for ($i=0; $i < $request['no_of_years'] ; $i++) { 
            $date = $request['date'.$i].'|'.$date;
            $total_revenue = $request['total_revenue'.$i].'|'.$total_revenue;
            $total_expense = $request['total_expense'.$i].'|'.$total_expense;
            $pat = $request['pat'.$i].'|'.$pat;
        }
        $getId = CompanyFinancial::where('report_id',$request['report_id'])->get();
        if (count($getId) == 0) {
            $insert_data = CompanyFinancial::insert(['report_id'=>$request['report_id'],
                                                    'date'=>$date,
                                                    'total_revenue'=>$total_revenue,
                                                    'total_expense'=>$total_expense,
                                                    'pat'=>$pat,
                                                    'financial_details'=>$request['financial_details']]);
            if ($insert_data) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
            }
        }else{
            $update_date = CompanyFinancial::where('report_id',$request['report_id'])->                               update(['report_id'=>$request['report_id'],
                                                    'date'=>$date,
                                                    'total_revenue'=>$total_revenue,
                                                    'total_expense'=>$total_expense,
                                                    'pat'=>$pat,
                                                    'financial_details'=>$request['financial_details']]);
            if ($update_date) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
            }

        }
    }
    public function addPromoter(Request $request)
    {
        $promoters = '';

        for ($i=0; $i < $request['no_of_promoters'] ; $i++) { 
            $promoters = $request['promoter'.$i].'|'.$promoters;
        }
        $getId = Promoter::where('report_id',$request['report_id'])->get();
        $offers = '';
        $fresh_issue = '';
        if (!empty($request['offers'])) {
            $offers = $request['offers'];
        }
        if (!empty($request['fresh_issue'])) {
            $fresh_issue = $request['fresh_issue'];
        }
        if (count($getId) == 0) {
            $insert_data = Promoter::insert(['report_id'=>$request['report_id'],
                                            'promoter_names'=>$promoters,
                                            'issue_details'=>$offers,
                                            'fresh_issue'=>$fresh_issue]);
            if ($insert_data) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
            }
        }else{
            $insert_data = Promoter::where('report_id',$request['report_id'])
                                    ->update(['promoter_names'=>$promoters,
                                            'issue_details'=>$offers,
                                            'fresh_issue'=>$fresh_issue]);
            if ($insert_data) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
            }
        }
    }
    public function addValuation(Request $request)
    {
        $getId = ValuantionReview::where('report_id',$request['report_id'])->get();
        if (count($getId) == 0) {
            $insert_data = ValuantionReview::insert(['report_id'=>$request['report_id'],
                                                    'valuantion_details'=>$request['valuation_details'],
                                                    'review'=>$request['review'],
                                                    'valuation_pe'=>$request['valuation_pe']
                                                ]);
            if ($insert_data) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
            }
        }else{
            $insert_data = ValuantionReview::where('report_id',$request['report_id'])->update(['valuantion_details'=>$request['valuation_details'],
                                                    'review'=>$request['review'],
                                                    'valuation_pe'=>$request['valuation_pe']
                                                ]);
            if ($insert_data) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
            }
        }
    }
    public function addBottomline(Request $request)
    {
        $getId = Bottomline::where('report_id',$request['report_id'])->get();
        if (count($getId) == 0) {
            $insert_data = Bottomline::insert(['report_id'=>$request['report_id'],
                                            'bottomlines'=>$request['bottomline'],
                                            'subscription'=>$request['suscription'],
                                            'percentage'=>$request['percentage']]);
            if ($insert_data) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
            }
        }else{
            $insert_data = Bottomline::where('report_id',$request['report_id'])
                                    ->update(['bottomlines'=>$request['bottomline'],
                                            'subscription'=>$request['suscription'],
                                            'percentage'=>$request['percentage']]);
            if ($insert_data) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0,'info'=>'Storing data has some problem.']);
            }
        }

    }

    public function getListed()
    {
        $today = Carbon::now();
        $listed = ListedIpo::pluck('report_id')->toArray();
        $reports = Report::where('listing_date','<=',$today)->whereNotIn('id',$listed)->get()->toArray();
        $data = [];
        $count = 0;
        foreach ($reports as $value) {
            $data[$count]['id'] = $value['id'];
            $data[$count]['ipo_name'] = $value['ipo_name'];
            $count += 1;
        }
        return response()->json(['msg'=>1,'ipos'=>$data]);
    }

    public function addListed(Request $request)
    {
        $listed = ListedIpo::where('report_id',$request['ipo-name'])->get()->toArray();
        $reports = Report::where('id',$request['ipo-name'])->get()->toArray();
        $price_band_to = $reports[0]['price_band_to'];
        $open_price = $request['open_price'];
        $premium = (($open_price - $price_band_to)/$price_band_to)*100;
        $premium = round($premium, 2);
        if (count($listed) == 0) {
            $insert = ListedIpo::insert(['report_id'=>$request['ipo-name'],
                                        'open_price'=>$open_price,
                                        'index_name'=>$request['index'],
                                        'index_code'=>$request['index_code'],
                                        'premium'=>$premium]);
        }else{
            $update = ListedIpo::where('report_id',$request['ipo-name'])->update([
                                        'open_price'=>$open_price,
                                        'index_name'=>$request['index'],
                                        'index_code'=>$request['index_code'],
                                        'premium'=>$premium]);
        }
        if ($request['index'] == 'bse') {
            $data = file_get_contents('http://www.bseindia.com/stock-share-price/SiteCache/EQHeaderData.aspx?text='.$request['index_code']);
            $data = explode('#', $data);
            $data = explode(',', $data[6]);
            // $data[0] -> Pre-close
            // $data[1] -> today open
            // $data[2] -> today high
            // $data[3] -> today low
            // $data[4] -> today close
            $close = $data[4];
            $returns = (($close - $open_price)/$open_price)*100;
            $returns = round($returns, 2);
            $update = ListedIpo::where('report_id',$request['ipo-name'])->update(['today_price'=>$close,'returns'=>$returns]);
            return response()->json(['msg'=>1]);

        }elseif ($request['index'] == 'nse') {
            $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$request['index_code'].'.NS?interval=1d');
            $data = json_decode($file);
            $values = $data->chart->result[0]->indicators->quote[0];
            $count = count($values->close) - 1;
            $close = round($values->close[$count], 2);
            $returns = (($close - $open_price)/$open_price)*100;
            $returns = round($returns, 2);
            $update = ListedIpo::where('report_id',$request['ipo-name'])->update(['today_price'=>$close,'returns'=>$returns]);
            return response()->json(['msg'=>1]);
        }

    }

    public function exportUpcoming()
    {
        $today = Carbon::now();
        $today = date('Y-m-d', strtotime($today));

        $reports = Report::where('issue_open_to','>=',$today)->get()->toArray();
        $upcomingipo = [];
        $count = 0;
        foreach ($reports as $value) {
            $upcomingipo[$count]['IPO Name'] = $value['ipo_name'];
            $upcomingipo[$count]['Open'] = $value['issue_open_from'];
            $upcomingipo[$count]['Close'] = $value['issue_open_to'];
            $upcomingipo[$count]['Issue size'] = $value['issue_size'];
            $upcomingipo[$count]['Listing Date'] = $value['listing_date'];
            $upcomingipo[$count]['Cut Of Price'] = $value['cut_of_price'];
            $upcomingipo[$count]['Valuation P/E'] = ValuantionReview::where('report_id',$value['id'])->value('valuation_pe');
            $upcomingipo[$count]['Subscription'] = Bottomline::where('report_id',$value['id'])->value('percentage');
            $count += 1;
        }

        Excel::create('upcoming-ipo', function($excel) use ($upcomingipo) {

            $excel->sheet('upcomingipo', function($sheet) use ($upcomingipo) {

                 $sheet->fromArray($upcomingipo);

            });

        })->export('xls');
    }

    public function exportListed()
    {
        $listed = ListedIpo::get()->toArray();
        $finalData = [];
        $count = 0;
        foreach ($listed as $ipo) {
            $report = Report::where('id',$ipo['report_id'])->get()->toArray();
            $finalData[$count]['IPO Name'] = $report[0]['ipo_name'];
            $finalData[$count]['Date'] = $report[0]['listing_date'];
            $finalData[$count]['Price From'] = $report[0]['price_band_from'];
            $finalData[$count]['Price To'] = $report[0]['price_band_to'];
            $finalData[$count]['Open Price'] = $ipo['open_price'];
            $finalData[$count]['Premium'] = $ipo['premium'];
            $finalData[$count]['Today Price'] = $ipo['today_price'];
            $finalData[$count]['Return'] = $ipo['returns'];
            $count += 1;
        }

        Excel::create('listed-ipo', function($excel) use ($finalData) {

            $excel->sheet('listedipo', function($sheet) use ($finalData) {

                 $sheet->fromArray($finalData);

            });

        })->export('xls');
    }

    public function setting()
    {
        $users = User::where('role','!=',1)->get()->toArray();

        return view('settings',['users'=>$users]);
    }

    public function addUser(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users',
            'password' => 'required',
            'mobile' => 'required|max:10|min:10|unique:users',
        ]);


        if ($validator->fails()) {
            return $validator->errors()->all();
        }

        $notification = 0;
        if (!empty($request['notification'])) {
            $notification = 1;
        }


        $insert = User::insert(['name'=>$request['name'],'email'=>$request['name'],'password'=>bcrypt($request['password']),'mobile'=>$request['mobile'],'notification'=>$notification]);
        if ($insert) {
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0]);
        }

    }

    // public function testtext()
    // {
    //     $datetime = new DateTime('tomorrow');
    //     $users = User::where('role','!=',1)->pluck('mobile')->toArray();
    //     $ipos = Report::where('listing_date',$datetime)->pluck('ipo_name')->toArray();
    //     $users = implode(",", $users);
    //     foreach ($ipos as $ipo) {
    //         $ch = curl_init();
    //         $user="naveen@rightfunds.com:Rightfunds20";
    //         $receipientno = $users;
    //         $senderID="RFIPOS";
    //         $msgtxt=$ipo." IPO will be listed tomorrow at 9.00AM."; 
    //         curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //         curl_setopt($ch, CURLOPT_POST, 1);
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
    //         $buffer = curl_exec($ch);
    //         curl_close($ch);
    //         var_dump($buffer);
    //     }
    //     dd('done');
    // }

    public function changePass(Request $request)
    {
        $old_pass = User::where('id',$request['id'])->value('password');
        if (Hash::check($request['old-pass'], $old_pass)) {
            $update = User::where('id',$request['id'])->update(['password'=>bcrypt($request['new-pass'])]);
            if ($update) {
                return response()->json(['msg'=>1, 'info'=>'Password Changed Successfully.']);
            }else{
                return response()->json(['msg'=>0,'info'=>'Someting went worng try again later.']);
            }
        }else{
            return response()->json(['msg'=>0,'info'=>'Old password is incorrect']);
        }
    }

    public function exportPdf($id)
    {   
        // $id = 1;
        $reports = Report::where('id',$id)->get()->toArray();
        $CompanyFinancial = CompanyFinancial::where('report_id',$id)->get()->toArray();
        $Promoter = Promoter::where('report_id',$id)->get()->toArray();
        $ValuantionReview = ValuantionReview::where('report_id',$id)->get()->toArray();
        $Bottomline = Bottomline::where('report_id',$id)->get()->toArray();

        if (empty($CompanyFinancial)) {
            $financial_table = '';
            $financial_details = '';
        }else{
            $financial_date = $CompanyFinancial[0]['date'];
            $financial_date = explode('|', $financial_date);
            $financial_revenue = $CompanyFinancial[0]['total_revenue'];
            $financial_revenue = explode('|', $financial_revenue);
            $financial_expense = $CompanyFinancial[0]['total_expense'];
            $financial_expense = explode('|', $financial_expense);
            $financial_pat = $CompanyFinancial[0]['pat'];
            $financial_pat = explode('|', $financial_pat);

            $financial_count = count($financial_date)-1;
            $financial_table = [];
            for ($i=0; $i < $financial_count; $i++) { 
                $financial_table[$i]['date'] = $financial_date[$i];
                $financial_table[$i]['revenue'] = $financial_revenue[$i];
                $financial_table[$i]['expense'] = $financial_expense[$i];
                $financial_table[$i]['pat'] = $financial_pat[$i];
            }
            $financial_details = $CompanyFinancial[0]['financial_details'];
        }

        if (empty($Promoter)) {
            $promoterNames = '';
            $issue_details = '';
            $fresh_issue = '';
        }else{
            $promoter_names = $Promoter[0]['promoter_names'];
            $promoter_names = explode('|', $promoter_names);

            $promoter_count = count($promoter_names)-1;
            $promoterNames = [];
            for ($i=0; $i < $promoter_count; $i++) { 
                $promoterNames[$i] = $promoter_names[$i];
            }
            $issue_details = $Promoter[0]['issue_details'];
            $fresh_issue = $Promoter[0]['fresh_issue'];
        }
        if (empty($Bottomline)) {
            $bottomlines = '';
        }else{
            $bottomlines = $Bottomline[0];
        }
        if (empty($ValuantionReview)) {
            $ValuantionReview = '';
        }else{
            $ValuantionReview = $ValuantionReview[0];
        }
        // dd($reports);
        $pdf = PDF::loadView('exportpdf',['reports'=>$reports,'financial_table'=>$financial_table,'financial_details'=>$financial_details,'promoter_names'=>$promoterNames,'issue_details'=>$issue_details,'fresh_issue'=>$fresh_issue,'val_rev'=>$ValuantionReview,'bottomline'=>$bottomlines]);
        return $pdf->download('exportpdf.pdf');
    }

    public function showPdf($id)
    {   
        // $id = 1;
        $reports = Report::where('id',$id)->get()->toArray();
        $CompanyFinancial = CompanyFinancial::where('report_id',$id)->get()->toArray();
        $Promoter = Promoter::where('report_id',$id)->get()->toArray();
        $ValuantionReview = ValuantionReview::where('report_id',$id)->get()->toArray();
        $Bottomline = Bottomline::where('report_id',$id)->get()->toArray();

        if (empty($CompanyFinancial)) {
            $financial_table = '';
            $financial_details = '';
        }else{
            $financial_date = $CompanyFinancial[0]['date'];
            $financial_date = explode('|', $financial_date);
            $financial_revenue = $CompanyFinancial[0]['total_revenue'];
            $financial_revenue = explode('|', $financial_revenue);
            $financial_expense = $CompanyFinancial[0]['total_expense'];
            $financial_expense = explode('|', $financial_expense);
            $financial_pat = $CompanyFinancial[0]['pat'];
            $financial_pat = explode('|', $financial_pat);

            $financial_count = count($financial_date)-1;
            $financial_table = [];
            for ($i=0; $i < $financial_count; $i++) { 
                $financial_table[$i]['date'] = $financial_date[$i];
                $financial_table[$i]['revenue'] = $financial_revenue[$i];
                $financial_table[$i]['expense'] = $financial_expense[$i];
                $financial_table[$i]['pat'] = $financial_pat[$i];
            }
            $financial_details = $CompanyFinancial[0]['financial_details'];
        }

        if (empty($Promoter)) {
            $promoterNames = '';
            $issue_details = '';
            $fresh_issue = '';
        }else{
            $promoter_names = $Promoter[0]['promoter_names'];
            $promoter_names = explode('|', $promoter_names);

            $promoter_count = count($promoter_names)-1;
            $promoterNames = [];
            for ($i=0; $i < $promoter_count; $i++) { 
                $promoterNames[$i] = $promoter_names[$i];
            }
            $issue_details = $Promoter[0]['issue_details'];
            $fresh_issue = $Promoter[0]['fresh_issue'];
        }
        if (empty($Bottomline)) {
            $bottomlines = '';
        }else{
            $bottomlines = $Bottomline[0];
        }
        if (empty($ValuantionReview)) {
            $ValuantionReview = '';
        }else{
            $ValuantionReview = $ValuantionReview[0];
        }
        // dd($reports);
        $pdf = PDF::loadView('exportpdf',['reports'=>$reports,'financial_table'=>$financial_table,'financial_details'=>$financial_details,'promoter_names'=>$promoterNames,'issue_details'=>$issue_details,'fresh_issue'=>$fresh_issue,'val_rev'=>$ValuantionReview,'bottomline'=>$bottomlines]);
        return $pdf->stream('exportpdf.pdf');
    }

    public function deleteUser(Request $request)
    {
        if (\Auth::user()->name == 'Admin') {
            $delete = User::where('id',$request['user_id'])->delete();
            if ($delete) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>2, 'info'=>'Something went worng.']);
            }
        }else{
            return response()->json(['msg'=>2,'info'=>'Only Admin can make the delete action.']);
        }
    }

    public function changeNotification(Request $request)
    {
        $update = User::where('id',$request['user_id'])->update(['notification'=>$request['change']]);
        if ($update) {
            return response()->json(['msg'=>1]);
        }else{
            return response()->json(['msg'=>0]);
        }
    }

    public function getUser(Request $request)
    {
        $data =  User::where('id',$request['id'])->get()->toArray();
        $val = ['name'=>$data[0]['name'],'mobile'=>$data[0]['mobile']];
        return response()->json(['msg'=>1,'val'=>$val]);
    }

    public function updateUser(Request $request)
    {
        $update = User::where('id',$request['id'])->update(['name'=>$request['name'],'email'=>$request['name'],'mobile'=>$request['mobile']]);
        return response()->json(['msg'=>1,'info'=>'User info updated.']);
    }

    public function getReport(Request $request)
    {
        $report = Report::where('id',$request['id'])->value('ipo_name');
        return response()->json(['name'=>$report]);
    }

    public function renameIpo(Request $request)
    {
        $update = Report::where('id',$request['id'])->update(['ipo_name'=>$request['ipo_name']]);
        if ($update) {
            return response()->json(['msg'=>1,'info'=>'Successfully updated.']);
        }
    }

    public function updateOpening(Request $request)
    {
        $listing = ListedIpo::where('report_id',$request['id'])->get()->toArray();
        $listing = $listing[0];
        $reports = Report::where('id',$request['id'])->get()->toArray();
        $price_band_to = $reports[0]['price_band_to'];
        $open_price = $request['open_price'];
        $premium = (($open_price - $price_band_to)/$price_band_to)*100;
        $premium = round($premium, 2);
            $update = ListedIpo::where('report_id',$request['id'])->update([
                                        'open_price'=>$open_price,
                                        'premium'=>$premium]);
        if ($listing['index_name'] == 'bse') {
            $data = file_get_contents('http://www.bseindia.com/stock-share-price/SiteCache/EQHeaderData.aspx?text='.$listing['index_code']);
            $data = explode('#', $data);
            $data = explode(',', $data[6]);
            $close = $data[4];
            $returns = (($close - $open_price)/$open_price)*100;
            $returns = round($returns, 2);
            $update = ListedIpo::where('report_id',$request['id'])->update(['today_price'=>$close,'returns'=>$returns]);
            return response()->json(['msg'=>1]);

        }elseif ($listing['index_name'] == 'nse') {
            $file = file_get_contents('https://query1.finance.yahoo.com/v8/finance/chart/'.$listing['index_code'].'.NS?interval=1d');
            $data = json_decode($file);
            $values = $data->chart->result[0]->indicators->quote[0];
            $count = count($values->close) - 1;
            $close = round($values->close[$count], 2);
            $returns = (($close - $open_price)/$open_price)*100;
            $returns = round($returns, 2);
            $update = ListedIpo::where('report_id',$request['id'])->update(['today_price'=>$close,'returns'=>$returns]);
            return response()->json(['msg'=>1]);
        }
    }
}
